﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ComputerStock.Models
{
    public class ProcessorTypeTable
    {
        public int id { get; set; }
        public string p_type_id { get; set; }
        public string p_company { get; set; }
        public string p_type { get; set; }
        public string p_name { get; set; }
        public string p_number { get; set; }
        public float clock_speed { get; set; }
        public string clock_speed_unit { get; set; }
        public int cache_memory { get; set; }
        public string cache_memory_unit { get; set; }
        public int cores { get; set; }
        public int threads { get; set; }
        public int  generation { get; set; }
        public int item_quantity { get; set; }

    }
}