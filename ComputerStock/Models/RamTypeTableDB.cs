﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Dapper;
using System.Data.SqlClient;
using System.Configuration;
using System.Data;

namespace ComputerStock.Models
{
    public class RamTypeTableDB
    {
        SqlConnection con = new SqlConnection(ConfigurationManager.ConnectionStrings["DefaultConnection"].ToString());

        #region ADMIN Side

        #region Get All Ram Types
        //--- Get All Ram Types ---//
        public List<RamTypeTable> GetRamTypes()
        {

            //using (IDbConnection conn = con)
            //{
            //    string query = "select * from ram_table";
            //    List<RamTable> ramTypeList = conn.Query<RamTable>(query).ToList<RamTable>();
            //    return ramTypeList;
            //}

            //using (IDbConnection conn = con)
            //{
            //    string spName = "GetAllRamTypes";
            //    List<RamTypeTable> ramTypeList = conn.Query<RamTypeTable>(spName, commandType: CommandType.StoredProcedure).ToList();
            //    return ramTypeList;
            //}

            using (IDbConnection conn = con)
            {
                string query = @"select * from RamTypeTable rt
                                 inner join ItemQuantityTable iq on iq.item_type_id=rt.ram_type_id";
                List<RamTypeTable> ramTypeList = conn.Query<RamTypeTable>(query).ToList();
                return ramTypeList;
            }
        }
        //--- END ---//
        #endregion

        #region Add New Ram Type
        //--- Add New Ram Type ---//
        public bool AddRamType(RamTypeTable model)
        {
            try
            {
                using (IDbConnection conn = con)
                {
                    string spName = "AddRamType";
                    var param = new DynamicParameters();
                    param.Add("ramType", model.ram_type, dbType: DbType.String);
                    param.Add("memorySize", model.memory_size, dbType: DbType.Int32);
                    param.Add("memoryUnit", model.memory_unit, dbType: DbType.String);
                    param.Add("clockSpeed", model.clock_speed, dbType: DbType.Int32);
                    param.Add("clockSpeedUnit", model.clock_speed_unit, dbType: DbType.String);
                    param.Add("isLaptopRam", model.is_laptop_ram, dbType: DbType.Boolean);

                    con.Execute(spName, param, commandType: CommandType.StoredProcedure);

                    return true;
                }
            }
            catch(Exception ex)
            {
                return false;
            }
        }
        //--- END ---//
        #endregion

        #region Delete Ram Type
        //--- Delete Ram Type ---//
        public bool DeleteRamType(string ramTypeId)
        {
            using (IDbConnection conn = con)
            {

                string spName = "DeleteRamType";

                var p = new DynamicParameters();
                p.Add("ramTypeId", ramTypeId, dbType: DbType.String);
                p.Add("deleteStatusBit", dbType: DbType.Boolean, direction: ParameterDirection.Output);

                con.Query(spName, p, commandType: CommandType.StoredProcedure);

                bool deleteStatusBit = p.Get<Boolean>("deleteStatusBit");

                return deleteStatusBit;
            }

        }
        //--- END ---//
        #endregion

        #endregion

        #region USER Side
//        public List<RamTypeTable> GetRAmByTypeNSize(decimal hddSize, string hddSubType)
//        {
//            byte isLaptopHDD = 0;

//            if (hddSize.ToString() == "2.5")
//            {
//                isLaptopHDD = 1;
//            }
//            else
//            {
//                isLaptopHDD = 0;
//            }

//            hddSubType = hddSubType.ToUpper();

//            using (IDbConnection conn = con)
//            {
//                string query = @"select * 
//                                 from HddTypeTable hdd
//                                 inner join ItemQuantityTable iq 
//                                 on iq.item_type_id=hdd.hdd_type_id
//                                 where hdd.hdd_type=@hddSubType and hdd.is_laptop_hdd=@isLaptopHDD";
//                List<HddTypeTable> hddTypeList = conn.Query<HddTypeTable>(query, new { hddSubType = hddSubType, isLaptopHDD = isLaptopHDD }).ToList();
//                return hddTypeList;
//            }
//        }


        #endregion


    }
}