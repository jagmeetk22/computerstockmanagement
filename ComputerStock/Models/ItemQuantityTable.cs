﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ComputerStock.Models
{
    public class ItemQuantityTable
    {
        public string item_type_id { get; set; }
        public int item_quantity { get; set; }

    }
}