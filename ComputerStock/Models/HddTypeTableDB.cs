﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using Dapper;
namespace ComputerStock.Models
{
    public class HddTypeTableDB
    {
        SqlConnection con = new SqlConnection(ConfigurationManager.ConnectionStrings["DefaultConnection"].ToString());

        #region ADMIN Side

        #region Get All HDD Types
        //--- Get All HDD Types ---//
        public List<HddTypeTable> GetHddTypes()
        {
            using (IDbConnection conn = con)
            {
                string query = @"select * from HddTypeTable hdd
                                 inner join ItemQuantityTable iq on iq.item_type_id=hdd.hdd_type_id";
                List<HddTypeTable> hddTypeList = conn.Query<HddTypeTable>(query).ToList();
                return hddTypeList;
            }
        }
        //--- END ---//
        #endregion

        #region Add New HDD Type
        //--- Add New HDD Type ---//
        public bool AddHddType(HddTypeTable model)
        {
            try
            {
                using (IDbConnection conn = con)
                {
                    string spName = "AddHddType";
                    var param = new DynamicParameters();
                    param.Add("hddType", model.hdd_type, dbType: DbType.String);
                    param.Add("memorySize", model.memory_size, dbType: DbType.Int32);
                    param.Add("memoryUnit", model.memory_unit, dbType: DbType.String);
                    param.Add("isLaptopHdd", model.is_laptop_hdd, dbType: DbType.Boolean);
                    param.Add("isSsd", model.is_ssd, dbType: DbType.Boolean);

                    con.Execute(spName, param, commandType: CommandType.StoredProcedure);

                    return true;
                }
            }
            catch (Exception ex)
            {
                Console.Write(ex.Message);
                return false;
            }
        }
        //--- END ---//
        #endregion

        #region Delete HDD Type
        //--- Delete HDD Type ---//
        public bool DeleteHddType(string hddTypeId)
        {
            using (IDbConnection conn = con)
            {

                string spName = "DeleteHddType";

                var p = new DynamicParameters();
                p.Add("hddTypeId", hddTypeId, dbType: DbType.String);
                p.Add("deleteStatusBit", dbType: DbType.Boolean, direction: ParameterDirection.Output);

                con.Query(spName, p, commandType: CommandType.StoredProcedure);

                bool deleteStatusBit = p.Get<Boolean>("deleteStatusBit");

                return deleteStatusBit;
            }

        }
        //--- END ---//
        #endregion

        #endregion AMIND Side

        #region USER Side
        public List<HddTypeTable> GetHddByTypeNSize(decimal hddSize, string hddSubType)
        {
            byte isLaptopHDD = 0;

            if (hddSize.ToString() == "2.5")
            {
                isLaptopHDD = 1;
            }
            else
            {
                isLaptopHDD = 0;
            }

            hddSubType = hddSubType.ToUpper();

            using (IDbConnection conn = con)
            {
                string query = @"select * 
                                 from HddTypeTable hdd
                                 inner join ItemQuantityTable iq 
                                 on iq.item_type_id=hdd.hdd_type_id
                                 where hdd.hdd_type=@hddSubType and hdd.is_laptop_hdd=@isLaptopHDD";
                List<HddTypeTable> hddTypeList = conn.Query<HddTypeTable>(query, new { hddSubType = hddSubType, isLaptopHDD = isLaptopHDD }).ToList();
                return hddTypeList;
            }
        }

        

        #endregion USER Side
    }
}






//HDD  >>  HDD SIZE (3.5"/2.5")  >>  HDD SUB TYPE (SATA/IDE)