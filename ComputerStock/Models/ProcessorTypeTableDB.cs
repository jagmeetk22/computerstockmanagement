﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using Dapper;

namespace ComputerStock.Models
{
    public class ProcessorTypeTableDB
    {
        SqlConnection con = new SqlConnection(ConfigurationManager.ConnectionStrings["DefaultConnection"].ToString());

        #region Get All Processor Types
        public List<ProcessorTypeTable> GetProcessorTypes()
        {
            using (IDbConnection conn = con)
            {
                string query = @"select * from ProcessorTypeTable pp
                                 inner join ItemQuantityTable iq on iq.item_type_id=pp.p_type_id";
                List<ProcessorTypeTable> pTypeList = conn.Query<ProcessorTypeTable>(query).ToList();
                return pTypeList;
            }
        }
        #endregion

        #region Add New Processor Type
        public bool AddProcessorType(ProcessorTypeTable model)
        {
            try

            {
                using (IDbConnection conn = con)
                {
                    string spName = "AddProcessorType";
                    var param = new DynamicParameters();
                    param.Add("pCompany", model.p_company, dbType: DbType.String);
                    param.Add("pType", model.p_type, dbType: DbType.String);
                    param.Add("pName", model.p_name, dbType: DbType.String);
                    param.Add("pNumber", model.p_number, dbType: DbType.String);
                    param.Add("clock_speed", model.clock_speed, dbType: DbType.Decimal);
                    param.Add("clock_speed_unit", model.clock_speed_unit, dbType: DbType.String);
                    param.Add("cache_memory", model.cache_memory, dbType: DbType.Int32);
                    param.Add("cache_memory_unit", model.cache_memory_unit, dbType: DbType.String);
                    param.Add("cores", model.cores, dbType: DbType.Int32);
                    param.Add("threads", model.threads, dbType: DbType.Int32);
                    param.Add("generation", model.generation, dbType: DbType.Int32);
  
                    con.Execute(spName, param, commandType: CommandType.StoredProcedure);

                    return true;
                }
            }
            catch (Exception ex)
            {
                return false;
            }
        }

        #endregion

        #region Delete Processor Type
        public bool DeleteProcessorType(string pTypeId)
        {
            using (IDbConnection conn = con)
            {

                string spName = "DeleteProcessorType";

                var p = new DynamicParameters();
                p.Add("pTypeId", pTypeId, dbType: DbType.String);
                p.Add("deleteStatusBit", dbType: DbType.Boolean, direction: ParameterDirection.Output);

                con.Query(spName, p, commandType: CommandType.StoredProcedure);

                bool deleteStatusBit = p.Get<Boolean>("deleteStatusBit");

                return deleteStatusBit;
            }

        }
        #endregion
    }
}