﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ComputerStock.Models
{
    public class RamTypeTable
    {
        public int id { get; set; }
        public string ram_type_id { get; set; }
        public string ram_type { get; set; }
        public int memory_size { get; set; }
        public string memory_unit { get; set; }
        public int clock_speed { get; set; }
        public bool is_laptop_ram { get; set; }
        public string clock_speed_unit { get; set; }
        public int item_quantity { get; set; }
    }
}
