﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ComputerStock.Models
{
    public class HddTypeTable
    {
        public int id { get; set; }
        public string hdd_type_id { get; set; }
        public string hdd_type { get; set; }
        public int memory_size { get; set; }
        public string memory_unit { get; set; }
        public bool is_laptop_hdd { get; set; }
        public bool is_ssd { get; set; }
        public int item_quantity { get; set; }

    }
}