﻿//---------------------------------------------------------------------//
//---------------------------------RAM SECTION-------------------------//
//---------------------------------------------------------------------//

//-----------RAM Type------------
$("body").on("click", "#ram_type", function () {
        $.ajax({
            method: 'GET',
            url: "/Home/RamTypePartial",
            success: function (response) {
                $(".header_bottom_right").html("");
                $(".header_bottom_right").html(response);
            }
        });
});


//-----------RAM Size Type------------
$("body").on("click", ".ram_size_type", function () {
    var ramSize = $(this).data("ram_size");
    $.ajax({
        method: 'GET',
        url: '/Home/RamSizeTypePartial',
        data: { ramSize: ramSize },
        success: function (response) {
            $(".header_bottom_right").html(response);
        }
    });
});

//-----------RAM SIZE Type Back------------
$("body").on("click", "#ram_size_type_back_btn", function () {
    $.ajax({
        method: 'GET',
        url: "/Home/RamTypePartial",
        success: function (response) {
            $(".header_bottom_right").html("");
            $(".header_bottom_right").html(response);
        }
    });
});

//-----------RAM DDR Type------------
$("body").on("click", ".ddr_type", function () {
    var ddrType = $(this).data("ddr_type");
    var ramSize = $(this).data("ram_size");
    $.ajax({
        method: 'GET',
        url: '/Home/RamDdrTypePartial',
        data: { ramSize: ramSize, ddrType: ddrType },
        success: function (response) {
            $(".header_bottom_right").html("");
            $(".header_bottom_right").html(response);
        }
    });
});

//-----------RAM DDR Type Back------------
$("body").on("click", "#ram_ddr_type_back_btn", function () {
    var ramSize = $(this).data("ram_size");
    $.ajax({
        method: 'GET',
        url: "/Home/RamSizeTypePartial",
        data:{ramSize:ramSize},
        success: function (response) {
            $(".header_bottom_right").html("");
            $(".header_bottom_right").html(response);
        }
    });
});


//---------------------------------------------------------------------//
//---------------------------------HDD SECTION-------------------------//
//---------------------------------------------------------------------//

//-----------HDD------------
$("#hdd_type").on("click", function () {
        $.ajax({
            method: 'GET',
            url: "/Home/HddTypePartial",
            success: function (response) {
                $(".header_bottom_right").html("");
                $(".header_bottom_right").html(response);
            }
        });
});

//-----------HDD SIZE------------
$("body").on("click", ".hdd_size", function () {
    var hddSize = $(this).data("hdd_size");
    console.log(hddSize);
    $.ajax({
        method: 'GET',
        url: "/Home/HddSizeTypePartial",
        data:{hddSize:hddSize},
        success: function (response) {
            $(".header_bottom_right").html("");
            $(".header_bottom_right").html(response);
        }
    });
});

//-----------Back From HDD SIZE------------
$("body").on("click", "#hdd_size_back_btn", function () {
    $.ajax({
        method: 'GET',
        url: "/Home/HddTypePartial",
        success: function (response) {
            $(".header_bottom_right").html("");
            $(".header_bottom_right").html(response);
        }
    });
});

//-----------HDD SUB TYPE------------
$("body").on("click", ".hdd_sub_type", function () {
    var hddSize = $(this).data("hdd_size");
    var hddSubType = $(this).data("hdd_sub_type");
    $.ajax({
        method: 'GET',
        url: "/Home/HddSubTypePartial",
        data: { hddSize: hddSize,hddSubType:hddSubType },
        success: function (response) {
            $(".header_bottom_right").html("");
            $(".header_bottom_right").html(response);
        }
    });
});

//-----------Back From HDD SUB TYPE------------
$("body").on("click", "#hdd_sub_type_back_btn", function () {
    var hddSize = $(this).data("hdd_size");
    $.ajax({
        method: 'GET',
        url: "/Home/HddSizeTypePartial",
        data:{hddSize:hddSize},
        success: function (response) {
            $(".header_bottom_right").html("");
            $(".header_bottom_right").html(response);
        }
    });
});


//---------------------------------------------------------------------//
//------------------------------PROCESSOR SECTION----------------------//
//---------------------------------------------------------------------//

//-----------PROCESSOR------------
$("#processor_type").on("click", function () {
    $(".header_bottom_right").fadeOut("slow");

    setTimeout(function () {
        $(".header_bottom_right").html("");
        $.ajax({
            method: 'GET',
            url: "/Home/ProcessorTypePartial",
            success: function (response) {
                console.log(response);
                $(".header_bottom_right").html(response);
            }
        });
    }, 500);

    setTimeout(function () {
        $(".header_bottom_right").fadeIn("slow");
    }, 1000);
});