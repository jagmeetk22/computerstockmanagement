﻿using ComputerStock.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace ComputerStock.Controllers
{
    [Authorize]
    public class AdminController : Controller
    {
        
        ItemQuantityTableDB itemQDbObj = new ItemQuantityTableDB();
        RamTypeTableDB ramDbObj = new RamTypeTableDB();
        HddTypeTableDB hddDbObj = new HddTypeTableDB();
        ProcessorTypeTableDB pDbObj = new ProcessorTypeTableDB();

        public ActionResult Dashboard()
        {
            return View();
        }


        //---Plus Minus
        #region Quantity Plus Minus
        public int ItemPlusOne(string itemTypeId)
        {
            int itemQuantity = itemQDbObj.ItemPlusOne(itemTypeId);
            return itemQuantity;
        }
        public int ItemMinusOne(string itemTypeId)
        {
            int itemQuantity = itemQDbObj.ItemMinusOne(itemTypeId);
            return itemQuantity;
        }
        #endregion

        //--- RAM
        #region RAM
        public ActionResult RamTypes(int? addRamResponse=0)
        {
            ViewBag.AddRamResponse = addRamResponse;

            RamTypeTableDB obj = new RamTypeTableDB();
            List<RamTypeTable> ramTypeList = obj.GetRamTypes();
            return View(ramTypeList);
        }
         [HttpPost]
        public ActionResult AddRamType(RamTypeTable model)
        {
            model.ram_type = model.ram_type.ToUpper();

            RamTypeTableDB obj = new RamTypeTableDB();
            bool response = obj.AddRamType(model);
             if (response==true)
             {
                 return RedirectToAction("RamTypes", new { addRamResponse  = 1});
             }
             else
             {
                 return RedirectToAction("RamTypes", new { addRamResponse = 2 });
             }
        }
        [HttpPost]
        public bool DeleteRamType(string ramTypeId)
         {
             RamTypeTableDB obj = new RamTypeTableDB();
             bool deleteStatus = obj.DeleteRamType(ramTypeId);
             return deleteStatus;
         }
        #endregion

        //--- HDD
        #region HDD
        public ActionResult HddTypes()
        {
            List<HddTypeTable> hddTypeList = hddDbObj.GetHddTypes();
            return View(hddTypeList);
        }
        public ActionResult AddHddType(HddTypeTable model)
        {
            model.hdd_type = model.hdd_type.ToUpper();

            bool response = hddDbObj.AddHddType(model);

            if (response == true)
            {
                return RedirectToAction("HddTypes");
            }
            else
            {
                return RedirectToAction("HddTypes");
            }
        }
        public bool DeleteHddType(string hddTypeId)
        {
            bool deleteStatus = hddDbObj.DeleteHddType(hddTypeId);
            return deleteStatus;
        }
        #endregion

        //--- PROCESSOR
        #region PROCESSOR
        public ActionResult ProcessorTypes()
        {
            List<ProcessorTypeTable> pTypeList = pDbObj.GetProcessorTypes();
            return View(pTypeList);
        }
        public ActionResult AddProcessorType(ProcessorTypeTable model)
        {
            model.p_number = model.p_number.ToUpper();

            bool response = pDbObj.AddProcessorType(model);

            if (response == true)
            {
                return RedirectToAction("ProcessorTypes");
            }
            else
            {
                return RedirectToAction("ProcessorTypes");
            }
        }
        public bool DeleteProcessorType(string pTypeId)
        {
            bool deleteStatus = pDbObj.DeleteProcessorType(pTypeId);
            return deleteStatus;
        }
        #endregion

        public ActionResult DataTable()
        {
            return View();
        }
    }
}