﻿using Microsoft.Owin;
using Owin;

[assembly: OwinStartupAttribute(typeof(ComputerStock.Startup))]
namespace ComputerStock
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            ConfigureAuth(app);
        }
    }
}
